var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function (){
    return 'id: ' + this.id + " | color: " + this.color;
}

    Bicicleta.allBicis = [];
    Bicicleta.add = function(aBici){
        Bicicleta.allBicis.push(aBici);
    }

    Bicicleta.findById = function(aBiciId){
        var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
        if (aBici)
            return aBici;
        else
            throw new Error(`No existe una Bicicleta con el id ${aBiciId}`);
    }

    Bicicleta.removeById = function(aBiciId){
        for(var i = 0; i < Bicicleta.allBicis.length; i++){
            if(Bicicleta.allBicis[i].id == aBiciId){
                Bicicleta.allBicis.splice(i, 1);
                break;
            }
        }
    }

    // var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
    // var b = new Bicicleta(2, 'blanca', 'urbana', [-34.596932, -58.3808287]);
    // var c = new Bicicleta(3, 'azul', 'urbana', [-34.6062424, -58.3821497]);
    // var d = new Bicicleta(4, 'verde', 'urbana', [-34.593232, -58.3895287]);

    // Bicicleta.add(a);
    // Bicicleta.add(b);
    // Bicicleta.add(c);
    // Bicicleta.add(d);

module.exports = Bicicleta;